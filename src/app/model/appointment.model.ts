export interface AppointmentAttributes {
  'guest-token'?: string;
  'agent-token'?: string;
  'meeting-point': boolean;
  'start-time': string;
  'end-time': string;
  'time-out-time'?: string;
  'status': string;
  'agent-login': string;
  'usecase-id'?: string;
  'name'?: string;
  'guest-display-name'?: string;
  'agent-display-name'?: string;
  'guest-default-url'?: string;
  'agent-default-url'?: string;
  'appointment-code'?: string | null;
}

export interface Appointment {
  id: string;
  type: string;
  links: {
    self?: string;
  };
  attributes: AppointmentAttributes;
}

export interface AppointmentResponse {
  data: Appointment[];
  links: {
    next?: string;
  };

}
