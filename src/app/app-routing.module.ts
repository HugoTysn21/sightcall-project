import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./component/login/login.component";
import {HomeComponent} from "./component/home/home.component";
import {AppointmentHandlerComponent} from "./component/appointment-handler/appointment-handler.component";
import {AuthGuard} from "./component/auth.guard";
import {AuthCallbackComponent} from "./component/auth-callback/auth-callback";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'auth-callback', component: AuthCallbackComponent },
  { path: 'client', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'handler', component: AppointmentHandlerComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
