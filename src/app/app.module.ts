import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppointmentHandlerComponent } from './component/appointment-handler/appointment-handler.component';
import {HttpClientModule} from "@angular/common/http";
import {AppointmentService} from "./service/appointment.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatCardModule} from "@angular/material/card";
import {AuthService} from "./service/auth-service";
import { HomeComponent } from './component/home/home.component';
import {LoginComponent} from "./component/login/login.component";
import {AuthCallbackComponent} from "./component/auth-callback/auth-callback";

@NgModule({
  declarations: [
    AppComponent,
    AppointmentHandlerComponent,
    LoginComponent,
    HomeComponent,
    AuthCallbackComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCardModule,
    MatDatepickerModule
  ],
  providers: [AppointmentService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
