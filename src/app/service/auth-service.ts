import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private clientId = '';
  private clientSecret = ''; //use your git
  private redirectUri = 'http://localhost:4200/auth-callback';
  private currentUser: { name: string, login: string, email: string, isAgent: boolean } | null = null;

  private credentials = [
    { name: 'Hugo Lantillon', login: 'hlantillon@gmail.com', password: 'test', isAgent: true },
    //{ name: 'Guest', login: 'guest', password: 'guestpass', isAgent: false }
  ];

  constructor(private router: Router, private http: HttpClient) {}

  login(login: string, password: string) {
    const user = this.credentials.find(cred => cred.login === login && cred.password === password);
    if (user) {
      this.currentUser = { name: user.name, login: user.login, email: '', isAgent: user.isAgent };
      this.router.navigate([user.isAgent ? '/handler' : '/client']);
      return true;
    }
    return false;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  githubLogin(code: string) {
    const url = `https://cors-anywhere.herokuapp.com/https://github.com/login/oauth/access_token`;
    return this.http.post(url, {
      client_id: this.clientId,
      client_secret: this.clientSecret,
      code: code,
      redirect_uri: this.redirectUri
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    });
  }

  setUserFromGitHub(accessToken: string) {
    const url = `https://cors-anywhere.herokuapp.com/https://api.github.com/user`;

    return this.http.get(url, {
      headers: new HttpHeaders({
        'Authorization': `token ${accessToken}`,
        'Accept': 'application/json'
      })
    }).subscribe((user: any) => {
      this.currentUser = { name: user.name, login: user.login, email: user.email, isAgent: false };
      console.log(this.currentUser)
      this.router.navigate(['/client']);
    });
  }

  loginWithGitHub() {
    const githubUrl = `https://github.com/login/oauth/authorize?client_id=${this.clientId}&redirect_uri=${this.redirectUri}`;
    window.location.href = githubUrl;
  }

  handleGitHubCallback(code: string) {
    this.githubLogin(code).subscribe((response: any) => {
      const accessToken = response.access_token;
      if (accessToken) {
        this.setUserFromGitHub(accessToken);
      } else {
        // Handle error if access token is not received
        console.error('Access token not received');
        this.router.navigate(['/login']);
      }
    }, error => {
      console.error('GitHub login failed', error);
      this.router.navigate(['/login']);
    });
  }
}
