import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {EMPTY, expand, filter, Observable, of, reduce} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Appointment, AppointmentAttributes, AppointmentResponse} from '../model/appointment.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  private apiUrl = 'https://appointment-ppr.sightcall.com/api/appointments';
  private apiKey = ''; // volontarily deletes due to security

  constructor(private http: HttpClient) {}

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Authorization': 'Token '+ this.apiKey,
      'Content-Type': 'application/vnd.api+json'
    });
  }

  getAppointments(agentLogin: string): Observable<Appointment[]> {
    return this.http.get<AppointmentResponse>(this.apiUrl, { headers: this.createHeaders() }).pipe(
      expand(response => response.links.next ? this.http.get<AppointmentResponse>(response.links.next, { headers: this.createHeaders() }) : EMPTY),
      map(response => {
        if (!response || Array.isArray(response)) {
          return [];
        }
        return response.data.filter((appt: Appointment) => appt.attributes['agent-login'] === agentLogin);
      }),
      //je combine enb un tabkeau
      reduce((acc, data) => acc.concat(data), [] as Appointment[]),
      catchError(error => {
        console.error('Error fetching appointments:', error);
        return of([]);
      })
    );
  }
  getAppointmentsByGuest(email: string): Observable<Appointment[]> {
    return this.http.get<AppointmentResponse>(this.apiUrl, { headers: this.createHeaders() }).pipe(
      expand(response => response.links.next ? this.http.get<AppointmentResponse>(response.links.next, { headers: this.createHeaders() }) : EMPTY),
      map(response => {
        if (!response || Array.isArray(response)) {
          return [];
        }
        return response.data.filter((appt: Appointment) => {
          const timeoutTimeStr = appt.attributes['time-out-time'];
          if (timeoutTimeStr) {
            const endTime = new Date(timeoutTimeStr);
            return appt.attributes['guest-display-name'] === email && endTime > new Date();
          }
          return false;
        });
      }),
      reduce((acc, data) => acc.concat(data), [] as Appointment[]),
      catchError(error => {
        console.error('Error fetching appointments by guest:', error);
        return of([]);
      })
    );
  }

  createAppointment(attributes: AppointmentAttributes): Observable<Appointment> {
    const appointment = {
      data: {
        type: 'appointments',
        attributes
      }
    };
    return this.http.post<{ data: Appointment }>(this.apiUrl, appointment, { headers: this.createHeaders() }).pipe(
      map(response => response.data)
    );
  }

  updateAppointment(id: string, attributes: AppointmentAttributes): Observable<Appointment> {
    const appointment = {
      data: {
        type: 'appointments',
        id,
        attributes
      }
    };
    return this.http.patch<{ data: Appointment }>(`${this.apiUrl}/${id}`, appointment, { headers: this.createHeaders() }).pipe(
      map(response => response.data)
    );
  }

  deleteAppointment(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`, { headers: this.createHeaders() });
  }
}
