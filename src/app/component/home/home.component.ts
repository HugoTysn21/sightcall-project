import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth-service';
import { Appointment } from '../../model/appointment.model';
import { AppointmentService } from '../../service/appointment.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  guestAppointments: Appointment[] = [];
  guestEmail: string | undefined;
  username: string = '';

  displayedColumns: string[] = ['guestName', 'date', 'time', 'guestUrl'];

  constructor(
    private authService: AuthService,
    private appointmentService: AppointmentService
  ) {}

  ngOnInit(): void {
    const currentUser = this.authService.getCurrentUser();
    if (currentUser) {
      this.guestEmail = currentUser.email;
      this.username = currentUser.login;
      this.loadGuestAppointments(this.guestEmail);
    }
  }

  loadGuestAppointments(email: string) {
    this.appointmentService.getAppointmentsByGuest(email).subscribe((appointments: Appointment[]) => {
      this.guestAppointments = appointments;
    });
  }
}
