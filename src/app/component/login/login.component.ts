import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../service/auth-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  login: string = '';
  password: string = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      const code = params['code'];
      if (code) {
        this.authService.handleGitHubCallback(code);
      }
    });
  }

  onLogin() {
    if (!this.authService.login(this.login, this.password)) {
      alert('Invalid credentials');
    }
  }

  onGitHubLogin() {
    this.authService.loginWithGitHub();
  }
}
