import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../service/auth-service';

@Component({
  selector: 'app-auth-callback',
  template: '<p>Authenticating...</p>'
})
export class AuthCallbackComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const code = params['code'];
      if (code) {
        this.authService.githubLogin(code).subscribe((response: any) => {
          const accessToken = response.access_token;
          if (accessToken) {
            this.authService.setUserFromGitHub(accessToken);
          } else {
            // Handle error if access token is not received
            console.error('Access token not received');
            this.router.navigate(['/login']);
          }
        }, error => {
          // Handle error during GitHub login
          console.error('GitHub login failed', error);
          this.router.navigate(['/login']);
        });
      } else {
        // Handle case where code is not present in the URL
        console.error('Authorization code not found');
        this.router.navigate(['/login']);
      }
    });
  }
}
