import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentHandlerComponent } from './appointment-handler.component';

describe('AppointmentHandlerComponent', () => {
  let component: AppointmentHandlerComponent;
  let fixture: ComponentFixture<AppointmentHandlerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppointmentHandlerComponent]
    });
    fixture = TestBed.createComponent(AppointmentHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
