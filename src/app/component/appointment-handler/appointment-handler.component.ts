import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Appointment, AppointmentAttributes } from '../../model/appointment.model';
import { AppointmentService } from '../../service/appointment.service';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '../../service/auth-service';

@Component({
  selector: 'app-appointment-handler',
  templateUrl: './appointment-handler.component.html',
  styleUrls: ['./appointment-handler.component.scss']
})
export class AppointmentHandlerComponent implements OnInit {
  appointmentForm: FormGroup;
  agentId = '';
  agentName = '';
  availableTimes: string[] = [];
  allTimes: string[] = [
    '09:00', '10:00', '11:00',
    '12:00', '13:00', '14:00', '15:00',
    '16:00', '17:00', '18:00'
  ];
  appointments$: Observable<Appointment[]> = of([]);
  dataSource = new MatTableDataSource<Appointment>([]);
  displayedColumns: string[] = ['guestName', 'date', 'time', 'guestUrl', 'actions'];
  selectedAppointment: Appointment | null = null;
  minDate: Date;
  currentTime: Date;

  constructor(
    private appointmentService: AppointmentService,
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.minDate = new Date();
    this.currentTime = new Date();
    this.appointmentForm = this.fb.group({
      guestName: ['', Validators.required],
      date: ['', Validators.required],
      time: ['', Validators.required]
    });

    this.updateMinDate();
    this.updateAvailableTimes();
  }

  ngOnInit(): void {
    const currentUser = this.authService.getCurrentUser();
    if (currentUser) {
      this.agentId = currentUser.login;
      this.agentName = currentUser.name;
    }
    this.loadAppointments();

    this.appointmentForm.get('date')?.valueChanges.subscribe(() => {
      this.updateMinDate();
      this.updateAvailableTimes();
      this.validateForm();
    });

    this.appointmentForm.get('time')?.valueChanges.subscribe(() => {
      this.validateForm();
    });

    setInterval(() => {
      this.currentTime = new Date();
      this.updateMinDate();
      this.updateAvailableTimes();
      this.validateForm();
    }, 60000); // Mettre à jour chaque minute
  }

  updateMinDate(): void {
    const currentDate = new Date(this.currentTime);
    const lastAvailableTime = this.allTimes[this.allTimes.length - 1];
    const [lastHour, lastMinute] = lastAvailableTime.split(':').map(Number);

    if (this.currentTime.getHours() > lastHour || (this.currentTime.getHours() === lastHour && this.currentTime.getMinutes() > lastMinute)) {
      currentDate.setDate(currentDate.getDate() + 1); // Passer au jour suivant si on est en dehors de la liste
    }

    this.minDate = currentDate;
  }

  updateAvailableTimes(): void {
    // Filtre les heures dispo en fonction de l'heure actuelle
    const selectedDate = this.appointmentForm.get('date')?.value ? new Date(this.appointmentForm.get('date')?.value) : null;
    const currentHour = this.currentTime.getHours();
    const currentMinutes = this.currentTime.getMinutes();
    const currentTimeString = `${currentHour}:${currentMinutes < 10 ? '0' + currentMinutes : currentMinutes}`;

    if (selectedDate && selectedDate.toDateString() === this.currentTime.toDateString()) {

      this.availableTimes = this.allTimes.filter(time => {
        const [hourStr, minuteStr] = time.split(':');
        const hour = parseInt(hourStr, 10);
        const minutes = parseInt(minuteStr, 10);
        return hour > currentHour || (hour === currentHour && minutes > currentMinutes);
      });
    } else {
      this.availableTimes = [...this.allTimes];
    }

    this.onDateChange();
  }

  loadAppointments(): void {
    this.appointmentService.getAppointments(this.agentId).pipe(
      map(appointments => {
        console.log(appointments)
        return appointments.filter(appt => {
          const timeoutTimeStr = appt.attributes['time-out-time'];
          if (timeoutTimeStr) {
            const timeoutTime = new Date(timeoutTimeStr);
            return timeoutTime > new Date();
          }
          return false;
        });
      }),
      catchError(error => {
        console.error('Error loading appointments:', error);
        return of([]);
      }),
      tap(appointments => {
        this.dataSource.data = appointments || [];
      })
    ).subscribe();
  }

  onDateChange(): void {
    const date = this.appointmentForm.get('date')?.value;
    if (date) {
      this.appointmentService.getAppointments(this.agentId).pipe(
        map(appointments => {
          const bookedTimes = appointments
            .filter(appt => appt.attributes['start-time'].startsWith(date))
            .map(appt => appt.attributes['start-time'].substring(11, 16));
          this.availableTimes = this.availableTimes.filter(time => !bookedTimes.includes(time));
          this.appointmentForm.get('time')?.updateValueAndValidity();
          return appointments;
        }),
        catchError(error => {
          console.error('Error fetching appointments:', error);
          return of([]);
        })
      ).subscribe();
    }
  }

  validateForm(): void {
    const selectedTime = this.appointmentForm.get('time')?.value;
    if (selectedTime && !this.availableTimes.includes(selectedTime)) {
      this.appointmentForm.get('time')?.setErrors({ invalid: true });
    } else {
      this.appointmentForm.get('time')?.setErrors(null);
    }
    this.appointmentForm.updateValueAndValidity();
  }

  createOrUpdateAppointment(): void {

    if (this.appointmentForm.invalid) {
      this.appointmentForm.markAllAsTouched();
      return;
    }

    const formValue = this.appointmentForm.value;
    const startTime = `${formValue.date}T${formValue.time}`;
    const endTime = this.calculateEndTime(startTime);

    console.log(`${formValue.date}T${formValue.time}`)
    console.log(startTime)
    const appointment: AppointmentAttributes = {
      'meeting-point': true,
      'start-time': startTime,
      'end-time': endTime,
      'time-out-time': endTime,
      'status': 'SCHEDULED',
      'agent-login': this.agentId,
      'usecase-id': '18804',
      'name': 'Appointment',
      'guest-display-name': formValue.guestName,
      'agent-display-name': this.agentName
    };

    this.appointmentService.getAppointments(this.agentId).pipe(
      switchMap(appointments => {
        const conflict = appointments.some(
          appt => appt.attributes['start-time'] === startTime &&
            appt.id !== this.selectedAppointment?.id
        );

        if (conflict) {
          console.log('This time slot is already booked.');
          return of(null);
        } else {
          if (this.selectedAppointment) {
            return this.appointmentService.updateAppointment(this.selectedAppointment.id, appointment).pipe(
              tap(() => {
                console.log('Appointment updated successfully!');
                this.loadAppointments();
                this.resetForm();
              })
            );
          } else {
            return this.appointmentService.createAppointment(appointment).pipe(
              tap(() => {
                console.log('Appointment created successfully!');
                this.loadAppointments();
                this.resetForm();
              })
            );
          }
        }
      }),
      catchError(error => {
        console.error('Error creating/updating appointment:', error);
        return of(null);
      })
    ).subscribe();
  }

  calculateEndTime(startTime: string): string {
    const startDate = new Date(startTime);
    startDate.setHours(startDate.getHours() + 1);
    return startDate.toISOString();
  }

  selectAppointment(appointment: Appointment): void {
    this.selectedAppointment = appointment;
    const [date, time] = appointment.attributes['start-time'].split('T');
    this.appointmentForm.patchValue({
      guestName: appointment.attributes['guest-display-name'],
      date: date,
      time: time.substring(0, 5)
    });

    // Forcer la vérification de la validité du formulaire après le pré-remplissage
    this.appointmentForm.updateValueAndValidity();
    this.validateForm();
    this.appointmentForm.markAllAsTouched();
  }

  resetForm(): void {
    this.appointmentForm.reset();
    this.selectedAppointment = null;
    this.availableTimes = [];
  }

  deleteAppointment(appointmentId: string): void {
    this.appointmentService.deleteAppointment(appointmentId).pipe(
      tap(() => this.loadAppointments()),
      catchError(error => {
        console.error('Error deleting appointment:', error);
        return of(null);
      })
    ).subscribe();
  }
}
